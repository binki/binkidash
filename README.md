# binkidash

Very simple example of how one might get started writing code to
interface with an ELM327. It unfortunately makes use of `alarm()`, but
it can (with duplication) display RPMs updated once per second.

#include <errno.h>
#include <inttypes.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

/*
 * For elmscan, use setserial to set the baud to 115200 (setserial
 * /dev/ttyUSB0 spd_vhi). Then run this program agains the thing to
 * see RPMs.
 */

/*
 * To get EINTR to happen, I have to provide an empty signal
 * handler. SIG_IGN doesn’t work.
 */
static void handle(int sig)
{
}

int main(int argc, const char *const argv[])
{
  FILE *elm;
  if (argc < 2)
    {
      fprintf(stderr, "must supply device path\n");
      return 1;
    }

  elm = fopen(argv[1], "r+b");
  if (!elm)
    {
      perror("fopen");
      return 1;
    }
  fprintf(elm, "AT L1\r\n");

  char *line = NULL;
  size_t line_n = 0;
  uint8_t b1 = 0, b2 = 0;
  uint16_t rpm = 0;
  while (1)
    {
      struct sigaction saction;
      struct sigaction saction_orig;
      /* Make getline() interruptable. */
      memset(&saction, 0, sizeof(saction));
      saction.sa_handler = handle;
      if (sigaction(SIGALRM, &saction, &saction_orig) == -1)
	{
	  perror("sigaction");
	  return 1;
	}
      alarm(1);
      ssize_t line_len = getline(&line, &line_n, elm);
      if (line_len == -1 && errno == EINTR)
	{
	  /*
	   * Was interrupted by SIGALRM, I bet. It just so happens
	   * that this pointlessly flags the stream as ferror()… So
	   * clear that.
	   */
	  clearerr(elm);
	}
      alarm(0);
      if (sigaction(SIGALRM, &saction_orig, NULL) == -1)
	{
	  perror("sigaction");
	  return 1;
	}

      if (line_len == -1)
	{
	  /* End of file/error in the stream? */
	  if (feof(elm))
	    {
	      fprintf(stderr, "EOF\n");
	      break;
	    }
	  if (ferror(elm))
	    {
	      fprintf(stderr, "Error reading from ELM327\n");
	      break;
	    }
	  /*
	   * Must have been interrupted by SIGALARM. Time to request
	   * data again.
	   */
	  fprintf(elm, "01 0C\r\n");
	}
      const char *rpm_prefix = "41 0C ";
      if (!strncmp(rpm_prefix, line, strlen(rpm_prefix)))
	{
	  /*
	   * chars after the prefix are hex RPM*4: 41 0C ff ff
	   */
	  sscanf(line, "41 0C %" SCNx8 " %" SCNx8 "", &b1, &b2);
	  rpm = (b1*256 + b2)/4;
	}

      printf("RPM: %d\n", rpm);
    }
  fclose(elm);

  return 0;
}
